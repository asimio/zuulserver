# README #

Spring Cloud Zuul Server's accompanying source code for blog post at http://tech.asimio.net/2017/10/10/Routing-requests-and-dynamically-refreshing-routes-using-Spring-Cloud-Zuul-Server.html

### Requirements ###

* Java 8
* Maven 3.3.x
* Docker host or Docker machine
* In case is desirable for Zuul Server maps routes to services from servicesIds, running instance(s) of Eureka Server as described at [https://bitbucket.org/asimio/discoveryserver/](https://bitbucket.org/asimio/discoveryserver/) would be needed.
* In case is desirable for Zuul Server to auto-reload routes as they are updated via `-Dspring.profiles.active=refreshable`, running instance(s) of Config Server (in **config-monitor** mode) and RabbitMQ as described at [https://bitbucket.org/asimio/configserver/](https://bitbucket.org/asimio/configserver/) would be needed.

### Building the artifact ###

```
mvn clean package
```

### Running Zuul service ###

Assumes two instances of Eureka Server as described at [https://bitbucket.org/asimio/discoveryserver/](https://bitbucket.org/asimio/discoveryserver/)
```
java -Dspring.profiles.active=refreshable -DappPort=8281 -DhostName=localhost -Deureka.client.serviceUrl.defaultZone="http://localhost:8001/eureka/,http://localhost:8002/eureka/" -jar target/zuul-server.jar
```

### How do I get set up using Docker? ###

```
sudo docker pull asimio/zuul-server
```

Assumes two instances of Eureka Server as described at [https://bitbucket.org/asimio/discoveryserver/](https://bitbucket.org/asimio/discoveryserver/)
```
sudo docker run -idt -p 8281:8281 -e appPort=8281 -e spring.profiles.active=refreshable -e hostName=localhost -e eureka.client.serviceUrl.defaultZone=http://localhost:8001/eureka/,http://localhost:8002/eureka/ asimio/zuul-server:latest
```

### Available endpoints

`http://localhost:8201/zuul1/actors/{id}`

### Who do I talk to? ###

* orlando.otero at asimio dot net
* [https://www.linkedin.com/in/ootero](https://www.linkedin.com/in/ootero)